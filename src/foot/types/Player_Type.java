package foot.types;

public class Player_Type {

	public enum MidField {
		LEFT_MIDFIELD, CENTER_MIDFIELD, RIGHT_MIDFIELD
	}

	public enum HostCountry {
		SPAIN, ITALY, GERMANY, ENGLAND, FRANCE
	}
	
	public enum Goal {
		PENALTY, FREE_KICK, OPEN_PLAY_GOAL, OWN_GOAL
	}
	
	public enum Defence {
		LEFT_BACK, CENTRE_BACK, RIGHT_BACK
	}
	
	public enum Attackers {
		LEFT_WING, CENTER_FORWARD, RIGHT_WING
	}

	String MidField() {
		
		return MidField().toString();
	}
}
