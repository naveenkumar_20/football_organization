package foot.players;

import foot.teams.Club;

public class Player {
	
	String name;
	short age;
	float height;
	float weight;
	String role;
	int assists;
	int goals;
	Club club;
	
}
